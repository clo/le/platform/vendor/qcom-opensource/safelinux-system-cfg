// SPDX-License-Identifier: GPL-2.0-only
/*
 * Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
 */

 /dts-v1/;

#include <dt-bindings/interrupt-controller/arm-gic.h>
#include <oot-dt-bindings/qcom-ipcc.h>

/ {
	model = "Qualcomm Technologies, Inc. SA8775P Virtual Platform";
	compatible = "qcom,sa8775p";
	qcom,board-id = <0x10029 0>;
	qcom,msm-id = <532 0x10000>, <533 0x10000>;

	interrupt-parent = <&intc>;
	#size-cells = <0x2>;
	#address-cells = <0x2>;

	firmware {
		scm {
			compatible = "qcom,scm";
		};
	};

	rng: rng@10d2000 {
		compatible = "qcom,sa8775p-trng", "qcom,trng";
		reg = <0x0 0x10d2000 0x0 0x1000>;
	};

	kaslr_store {
		compatible = "qcom,kaslr-off-store";
		reg = <0 0x90880000 0 0x10>;
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		sail_ss_mem: sail_ss_region@80000000 {
			no-map;
			reg = <0x0 0x80000000 0x0 0x10000000>;
		};

		hyp_mem: hyp_region@90000000 {
			no-map;
			reg = <0x0 0x90000000 0x0 0x600000>;
		};

		xbl_boot_mem: xbl_boot_region@90600000 {
			no-map;
			reg = <0x0 0x90600000 0x0 0x200000>;
		};

		aop_image_mem: aop_image_region@90800000 {
			no-map;
			reg = <0x0 0x90800000 0x0 0x60000>;
		};

		aop_cmd_db_mem: aop_cmd_db_region@90860000 {
			compatible = "qcom,cmd-db";
			no-map;
			reg = <0x0 0x90860000 0x0 0x20000>;
		};

		kaslr_off_reg: kasr_off_reg@90880000 {
			reg = <0 0x90880000 0 0x10>;
			no-map;
		};

		uefi_log: uefi_log_region@908b0000 {
			no-map;
			reg = <0x0 0x908b0000 0x0 0x10000>;
		};

		reserved_mem: reserved_region@908f0000 {
			no-map;
			reg = <0x0 0x908f0000 0x0 0xf000>;
		};

		secdata_apss_mem: secdata_apss_region@908ff000 {
			no-map;
			reg = <0x0 0x908ff000 0x0 0x1000>;
		};

		smem_mem: smem_region@90900000 {
			compatible = "qcom,smem";
			reg = <0x0 0x90900000 0x0 0x200000>;
			no-map;
			hwlocks = <&tcsr_mutex 3>;
		};

		cpucp_fw_mem: cpucp_fw_region@90b00000 {
			no-map;
			reg = <0x0 0x90b00000 0x0 0x100000>;
		};

		lpass_machine_learning_mem: lpass_machine_learning_region@93b00000 {
			no-map;
			reg = <0x0 0x93b00000 0x0 0xf00000>;
		};

		adsp_rpc_remote_heap_mem: adsp_rpc_remote_heap_region@94a00000 {
			no-map;
			reg = <0x0 0x94a00000 0x0 0x800000>;
		};

		pil_camera_mem: pil_camera_region@95200000 {
			no-map;
			reg = <0x0 0x95200000 0x0 0x500000>;
		};

		pil_adsp_mem: pil_adsp_region@95c00000 {
			no-map;
			reg = <0x0 0x95c00000 0x0 0x1e00000>;
		};

		pil_gdsp0_mem: pil_gdsp0_region@97b00000 {
			no-map;
			reg = <0x0 0x97b00000 0x0 0x1e00000>;
		};

		pil_gdsp1_mem: pil_gdsp1_region@99900000 {
			no-map;
			reg = <0x0 0x99900000 0x0 0x1e00000>;
		};

		pil_cdsp0_mem: pil_cdsp0_region@9b800000 {
			no-map;
			reg = <0x0 0x9b800000 0x0 0x1e00000>;
		};

		pil_gpu_mem: pil_gpu_region@9d600000 {
			no-map;
			reg = <0x0 0x9d600000 0x0 0x2000>;
		};

		pil_cdsp1_mem: pil_cdsp1_region@9d700000 {
			no-map;
			reg = <0x0 0x9d700000 0x0 0x1e00000>;
		};

		pil_cvp_mem: pil_cvp_region@9f500000 {
			no-map;
			reg = <0x0 0x9f500000 0x0 0x700000>;
		};

		pil_video_mem: pil_video_region@9fc00000 {
			no-map;
			reg = <0x0 0x9fc00000 0x0 0x700000>;
		};

		reserved_mem_u: pilu_qseecom@adc00000 {
			no-map;
			reg = <0x0 0xadc00000 0x0 0x1400000>;
		};

		hyptz_reserved_mem: hyptz_reserved_region@beb00000 {
			no-map;
			reg = <0x0 0xbeb00000 0x0 0x11500000>;
		};

		tz_stat_mem: tz_stat_region@d0000000 {
			no-map;
			reg = <0x0 0xd0000000 0x0 0x100000>;
		};

		tags_mem: tags_region@d0100000 {
			no-map;
			reg = <0x0 0xd0100000 0x0 0x1200000>;
		};

		qtee_mem: qtee_region@d1300000 {
			no-map;
			reg = <0x0 0xd1300000 0x0 0x500000>;
		};

		trusted_apps_mem: trusted_apps_region@d1800000 {
			no-map;
			reg = <0x0 0xd1800000 0x0 0x3900000>;
		};

		scmi_mem: scmi-region@d0000000 {
			no-map;
			reg = <0x0 0xd0000000 0x0 0x40000>;
		};

		auto_vm_vblk0_ring: auto_vm_vblk0_ring@d5100000 {
			no-map;
			reg = <0x0 0xd5100000 0x0 0x4000>;
			gunyah-label = <0x10>;
		};

		auto_vm_vblk1_ring: auto_vm_vblk1_ring@d5104000 {
			no-map;
			reg = <0x0 0xd5104000 0x0 0x4000>;
			gunyah-label = <0x11>;
		};

		auto_vm_vblk2_ring: auto_vm_vblk2_ring@d5108000 {
			no-map;
			reg = <0x0 0xd5108000 0x0 0x4000>;
			gunyah-label = <0x15>;
		};

		auto_vm_vblk3_ring: auto_vm_vblk3_ring@d510c000 {
			no-map;
			reg = <0x0 0xd510c000 0x0 0x4000>;
			gunyah-label = <0x16>;
		};

		auto_vm_vblk4_ring: auto_vm_vblk4_ring@d5110000 {
			no-map;
			reg = <0x0 0xd5110000 0x0 0x4000>;
			gunyah-label = <0x17>;
		};

		auto_vm_vnet0_ring: auto_vm_vnet0_ring@d5114000 {
			no-map;
			reg = <0x0 0xd5114000 0x0 0xC000>;
			gunyah-label = <0x13>;
		};

		auto_vm_vhab0_ring: auto_vm_vhab0_ring@d5120000 {
			no-map;
			reg = <0x0 0xd5120000 0x0 0x20000>;
			gunyah-label = <0x14>;
		};

		auto_vm_vhab1_ring: auto_vm_vhab1_ring@d5140000 {
			no-map;
			reg = <0x0 0xd5140000 0x0 0x20000>;
			gunyah-label = <0x18>;
		};

		auto_vm_vhab2_ring: auto_vm_vhab2_ring@d5160000 {
			no-map;
			reg = <0x0 0xd5160000 0x0 0x80000>;
			gunyah-label = <0x19>;
		};

		auto_vm_vhab3_ring: auto_vm_vhab3_ring@d51e0000 {
			no-map;
			reg = <0x0 0xd51e0000 0x0 0xa0000>;
			gunyah-label = <0x1a>;
		};

		auto_vm_vhab4_ring: auto_vm_vhab4_ring@d5280000 {
			no-map;
			reg = <0x0 0xd5280000 0x0 0x60000>;
			gunyah-label = <0x1b>;
		};

		auto_vm_vhab5_ring: auto_vm_vhab5_ring@d52e0000 {
			no-map;
			reg = <0x0 0xd52e0000 0x0 0x60000>;
			gunyah-label = <0x1c>;
		};

		auto_vm_input0_ring: auto_vm_input0_ring@d5340000 {
			no-map;
			reg = <0x0 0xd5340000 0x0 0x8000>;
			gunyah-label = <0x1d>;
		};

		auto_vm_vscmi_ring: auto_vm_vscmi_ring@d5348000 {
			no-map;
			reg = <0x0 0xd5348000 0x0 0x8000>;
			gunyah-label = <0x1e>;
		};

		auto_vm_vconsole_ring: auto_vm_vconsole_ring@d5350000 {
			no-map;
			reg = <0x0 0xd5350000 0x0 0x8000>;
			gunyah-label = <0x1f>;
		};

		auto_vm_input1_ring: auto_vm_input1_ring@d5358000 {
			no-map;
			reg = <0x0 0xd5358000 0x0 0x8000>;
			gunyah-label = <0x20>;
		};

		auto_vm_resv_ring: auto_vm_resv_ring@d5360000 {
			no-map;
			reg = <0x0 0xd5360000 0x0 0xa0000>;
			gunyah-label = <0x21>;
		};

		auto_vm_swiotlb: auto_vm_swiotlb@d5400000 {
			no-map;
			reg = <0x0 0xd5400000 0x0 0x1000000>;
			gunyah-label = <0x12>;
		};

		auto_vm_0_mem_0: auto_vm_0_mem_0@e00000000 {
			no-map;
			reg = <0x0 0xe0000000 0x0 0x20000000>;
		};

		auto_vm_0_mem_1: auto_vm_0_mem_1@1800000000 {
			no-map;
			reg = <0x01 0x80000000 0x0 0xc0000000>;
		};

		auto_vm_0_mem_2: auto_vm_0_mem_2@240000000 {
			no-map;
			reg = <0x02 0x40000000 0x0 0xc0000000>;
		};

		auto_vm_0_mem_3: auto_vm_0_mem_3@a300000000 {
			no-map;
			reg = <0x0a 0x30000000 0x0 0x50000000>;
		};

		auto_vm_0_mem_4: auto_vm_0_mem_4@a80000000 {
			no-map;
			reg = <0x0a 0x80000000 0x0 0xc0000000>;
		};

		auto_vm_0_mem_5: auto_vm_0_mem_5@b40000000 {
			no-map;
			reg = <0x0b 0x40000000 0x0 0x80000000>;
		};

		auto_vm_0_mem_6: auto_vm_0_mem_6@e000000000 {
			no-map;
			reg = <0x0e 0x00000000 0x0 0xc0000000>;
		};

		dump_mem: mem_dump_region {
			compatible = "shared-dma-pool";
			alloc-ranges = <0x0 0x00000000 0x0 0xffffffff>;
			reusable;
			size = <0 0x3000000>;
		};

		/* global autoconfigured region for contiguous allocations */
		linux,cma {
			compatible = "shared-dma-pool";
			alloc-ranges = <0x0 0x00000000 0x0 0xdfffffff>;
			reusable;
			alignment = <0x0 0x400000>;
			size = <0x0 0x2000000>;
			linux,cma-default;
		};
	};
	soc: soc { };

	memory@80000000 {
		reg = <0x00 0x80000000 0x00 0x3EE00000 0x00 0xc0000000 0x00 0x40000000 0x0d 0x00 0x02 0x52c00000 0x00000001 0x00000000 0x00000003 0x00000000>;
		device_type = "memory";
	};

	platform@c000000 {
		interrupt-parent = <&intc>;
		ranges = <0x0 0x0 0xc000000 0x2000000>;
		#address-cells = <0x1>;
		#size-cells = <0x1>;
		compatible = "qemu,platform", "simple-bus";
	};

	/* virtio block for rootfs */
	virtio_blk_1:virtio_mmio@1d84000 {
		dma-coherent;
		interrupts = <GIC_SPI 236 IRQ_TYPE_LEVEL_HIGH>;
		reg = <0x0 0x1d84000 0x0 0x2000>;
		compatible = "virtio,mmio";
	};

	/* virtio block for v-disk */
	virtio_blk_2:virtio_mmio@1d88000 {
		dma-coherent;
		interrupts = <GIC_SPI 238 IRQ_TYPE_LEVEL_HIGH>;
		reg = <0x0 0x1d88000 0x0 0x2000>;
		compatible = "virtio,mmio";
	};

	virtio_net:virtio_mmio@23000000 {
		dma-coherent;
		interrupts = < GIC_SPI 558 IRQ_TYPE_LEVEL_HIGH>;
		reg = <0x0 0x23000000 0x0 0x10000>;
		compatible = "virtio,mmio";
	};

	gpio-keys {
		#address-cells = <0x1>;
		#size-cells = <0x0>;
		compatible = "gpio-keys";

		poweroff {
			gpios = <0x8002 0x3 0x0>;
			linux,code = <0x74>;
			label = "GPIO Key Poweroff";
		};
	};

	pcie@1c10000 {
		interrupt-map-mask = <0x1800 0x0 0x0 0x7>;
		interrupt-map = <0x0 0x0 0x0 0x1 &intc 0x0 0x0 GIC_SPI 541 IRQ_TYPE_LEVEL_HIGH>,
				<0x0 0x0 0x0 0x2 &intc 0x0 0x0 GIC_SPI 542 IRQ_TYPE_LEVEL_HIGH>,
				<0x0 0x0 0x0 0x3 &intc 0x0 0x0 GIC_SPI 543 IRQ_TYPE_LEVEL_HIGH>,
				<0x0 0x0 0x0 0x4 &intc 0x0 0x0 GIC_SPI 544 IRQ_TYPE_LEVEL_HIGH>,
				<0x800 0x0 0x0 0x1 &intc 0x0 0x0 GIC_SPI 542 IRQ_TYPE_LEVEL_HIGH>,
				<0x800 0x0 0x0 0x2 &intc 0x0 0x0 GIC_SPI 543 IRQ_TYPE_LEVEL_HIGH>,
				<0x800 0x0 0x0 0x3 &intc 0x0 0x0 GIC_SPI 544 IRQ_TYPE_LEVEL_HIGH>,
				<0x800 0x0 0x0 0x4 &intc 0x0 0x0 GIC_SPI 541 IRQ_TYPE_LEVEL_HIGH>,
				<0x1000 0x0 0x0 0x1 &intc 0x0 0x0 GIC_SPI 543 IRQ_TYPE_LEVEL_HIGH>,
				<0x1000 0x0 0x0 0x2 &intc 0x0 0x0 GIC_SPI 544 IRQ_TYPE_LEVEL_HIGH>,
				<0x1000 0x0 0x0 0x3 &intc 0x0 0x0 GIC_SPI 541 IRQ_TYPE_LEVEL_HIGH>,
				<0x1000 0x0 0x0 0x4 &intc 0x0 0x0 GIC_SPI 542 IRQ_TYPE_LEVEL_HIGH>,
				<0x1800 0x0 0x0 0x1 &intc 0x0 0x0 GIC_SPI 544 IRQ_TYPE_LEVEL_HIGH>,
				<0x1800 0x0 0x0 0x2 &intc 0x0 0x0 GIC_SPI 541 IRQ_TYPE_LEVEL_HIGH>,
				<0x1800 0x0 0x0 0x3 &intc 0x0 0x0 GIC_SPI 542 IRQ_TYPE_LEVEL_HIGH>,
				<0x1800 0x0 0x0 0x4 &intc 0x0 0x0 GIC_SPI 543 IRQ_TYPE_LEVEL_HIGH>;
		#interrupt-cells = <0x1>;
		ranges = <0x1000000 0x0 0x60200000 0x0 0x60200000
			0x0 0x100000 0x2000000 0x0 0x60300000
			0x0 0x60300000 0x0 0x1fd00000 0x3000000
			0x4 0x0 0x4 0x0 0x2 0x0>;

		reg = <0x0 0x43B50000 0x0 0x10000000>;
		dma-coherent;
		bus-range = <0x0 0xff>;
		linux,pci-domain = <0x0>;
		#size-cells = <0x2>;
		#address-cells = <0x3>;
		device_type = "pci";
		compatible = "pci-host-ecam-generic";
	};

	pl011@40000000  {
		clock-names = "uartclk", "apb_pclk";
		clocks = <0x8000 0x8000>;
		interrupts = <GIC_SPI 0x17B IRQ_TYPE_LEVEL_HIGH>;
		reg = <0x0 0x40000000 0x0 0x1000>;
		compatible = "arm,pl011", "arm,primecell";
	};

	hypervisor {
		#address-cells = <0x02>;
		#size-cells = <0x00>;
		compatible = "qcom,gunyah-hypervisor-1.0","qcom,gunyah-hypervisor", "simple-bus";

		qcom,gunyah-vm {
			qcom,vendor = "Qualcomm";
			compatible = "qcom,gunyah-vm-id-1.0","qcom,gunyah-vm-id";
			qcom,vmid = <0x03>;
		};

		qcom,resource-manager-rpc@0000000000000000 {
			compatible = "gunyah-resource-manager","qcom,resource-manager-1-0","qcom,resource-manager","qcom,gunyah-message-queue","qcom,gunyah-capability";
			reg = <0x00 0x00 0x00 0x01>;
			interrupts = <GIC_SPI 0x3c0 IRQ_TYPE_EDGE_RISING GIC_SPI 0x3c1 IRQ_TYPE_EDGE_RISING>;
			qcom,free-irq-start = <0x3c0>;
			qcom,is-full-duplex;
			qcom,tx-message-size = <0xf0>;
			qcom,rx-message-size = <0xf0>;
			qcom,tx-queue-depth = <0x08>;
			qcom,rx-queue-depth = <0x08>;
		};
	};
	cpus {
		#size-cells = <0x0>;
		#address-cells = <0x2>;

		cpu@0 {
			reg = <0x0 0x0>;
			enable-method = "psci";
			compatible = "arm,cortex-a57";
			device_type = "cpu";
		};

		cpu@1 {
			reg = <0x0 0x1>;
			enable-method = "psci";
			compatible = "arm,cortex-a57";
			device_type = "cpu";
		};

		cpu@2 {
			reg = <0x0 0x2>;
			enable-method = "psci";
			compatible = "arm,cortex-a57";
			device_type = "cpu";
		};

		cpu@3 {
			reg = <0x0 0x3>;
			enable-method = "psci";
			compatible = "arm,cortex-a57";
			device_type = "cpu";
		};

		cpu@4 {
			reg = <0x0 0x4>;
			enable-method = "psci";
			compatible = "arm,cortex-a57";
			device_type = "cpu";
		};

		cpu@5 {
			reg = <0x0 0x5>;
			enable-method = "psci";
			compatible = "arm,cortex-a57";
			device_type = "cpu";
		};

		cpu@6 {
			reg = <0x0 0x6>;
			enable-method = "psci";
			compatible = "arm,cortex-a57";
			device_type = "cpu";
		};

		cpu@7 {
			reg = <0x0 0x7>;
			enable-method = "psci";
			compatible = "arm,cortex-a57";
			device_type = "cpu";
		};
	};

	apb-pclk {
		phandle = <0x8000>;
		clock-output-names = "clk24mhz";
		clock-frequency = <0x16e3600>;
		#clock-cells = <0x0>;
		compatible = "fixed-clock";
	};

	chosen {
		bootargs = "rw root=/dev/vda earlyprintk=serial,ttyAMA0 console=ttyAMA0 firmware_class.path=/firmware";
		stdout-path = "/pl011@40000000";
		linux,initrd-start = <0xbd151000>;
		linux,initrd-end =  <0xbef00000>;
		kaslr-seed = <0x0 0x0>;
	};

};

&soc {
	#address-cells = <2>;
	#size-cells = <2>;
	ranges = <0 0 0 0 0x10 0>;
	compatible = "simple-bus";

	psci {
		migrate = <0xc4000005>;
		cpu_on = <0xc4000003>;
		cpu_off = <0x84000002>;
		cpu_suspend = <0xc4000001>;
		method = "smc";
		compatible = "arm,psci-1.0", "arm,psci";
	};

	intc: interrupt-controller@17A00000 {
		compatible = "arm,gic-v3";
		#interrupt-cells = <3>;
		interrupt-controller;
		#address-cells = <0x2>;
		#size-cells = <0x2>;
		ranges;
		#redistributor-regions = <1>;
		redistributor-stride = <0x0 0x20000>;
		reg = <0x0 0x17a00000 0x0 0x10000>,	 /* GICD */
			  <0x0 0x17a60000 0x0 0x100000>;	/* GICR * 8 */
		interrupts = <GIC_PPI 9 IRQ_TYPE_LEVEL_HIGH>;
	};

	arch_timer: timer {
		compatible = "arm,armv8-timer", "arm,armv7-timer";
		interrupts = <GIC_PPI 13 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_PPI 14 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_PPI 11 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_PPI 12 IRQ_TYPE_LEVEL_HIGH>;
		always-on;
	};

	memtimer: timer@17c20000 {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;
		compatible = "arm,armv7-timer-mem";
		reg = <0x0 0x17c20000 0x0 0x1000>;
		clock-frequency = <19200000>;

		frame@17c21000 {
			frame-number = <0>;
			interrupts = <GIC_SPI 8 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 6 IRQ_TYPE_LEVEL_HIGH>;
			reg = <0x17c21000 0x1000>,
			      <0x17c22000 0x1000>;
		};

		frame@17c23000 {
			frame-number = <1>;
			interrupts = <GIC_SPI 9 IRQ_TYPE_LEVEL_HIGH>;
			reg = <0x17c23000 0x1000>;
			status = "disabled";
		};

		frame@17c25000 {
			frame-number = <2>;
			interrupts = <GIC_SPI 10 IRQ_TYPE_LEVEL_HIGH>;
			reg = <0x17c25000 0x1000>;
			status = "disabled";
		};

		frame@17c27000 {
			frame-number = <3>;
			interrupts = <GIC_SPI 11 IRQ_TYPE_LEVEL_HIGH>;
			reg = <0x17c27000 0x1000>;
			status = "disabled";
		};

		frame@17c29000 {
			frame-number = <4>;
			interrupts = <GIC_SPI 12 IRQ_TYPE_LEVEL_HIGH>;
			reg = <0x17c29000 0x1000>;
			status = "disabled";
		};

		frame@17c2b000 {
			frame-number = <5>;
			interrupts = <GIC_SPI 13 IRQ_TYPE_LEVEL_HIGH>;
			reg = <0x17c2b000 0x1000>;
			status = "disabled";
		};

		frame@17c2d000 {
			frame-number = <6>;
			interrupts = <GIC_SPI 14 IRQ_TYPE_LEVEL_HIGH>;
			reg = <0x17c2d000 0x1000>;
			status = "disabled";
		};
	};

	apps_smmu: apps-smmu@15000000 {
		compatible = "qcom,sa8755p-v500", "arm,mmu-500";
		reg = <0x0 0x15000000 0x0 0x100000>,
		      <0x0 0x15182000 0x0 0x28>;
		reg-names = "base", "tcu-base";
		#iommu-cells = <2>;
		qcom,skip-init;
		qcom,use-3-lvl-tables;
		#global-interrupts = <2>;
		#size-cells = <1>;
		#address-cells = <1>;
		ranges;

		interrupts = <GIC_SPI 119 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 120 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 102 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 103 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 104 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 105 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 106 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 107 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 108 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 109 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 110 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 111 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 112 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 113 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 114 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 115 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 116 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 117 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 118 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 181 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 182 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 183 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 184 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 185 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 186 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 187 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 188 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 189 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 190 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 191 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 192 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 315 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 316 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 317 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 318 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 319 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 320 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 321 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 322 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 323 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 324 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 325 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 326 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 327 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 328 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 329 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 330 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 331 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 332 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 333 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 334 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 335 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 336 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 337 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 338 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 339 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 340 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 341 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 342 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 343 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 344 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 345 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 395 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 396 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 397 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 398 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 399 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 400 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 401 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 402 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 403 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 404 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 405 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 406 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 407 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 408 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 409 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 418 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 419 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 412 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 421 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 706 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 423 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 424 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 425 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 689 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 690 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 691 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 692 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 693 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 694 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 695 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 696 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 410 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 411 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 420 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 413 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 422 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 707 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 708 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 709 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 710 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 711 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 414 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 712 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 713 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 714 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 715 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 912 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 911 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 910 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 909 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 908 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 907 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 906 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 905 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 904 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 903 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 902 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 901 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 900 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 899 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 898 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 897 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 896 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 895 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 894 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 893 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 892 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 891 IRQ_TYPE_LEVEL_HIGH>;

		anoc_1_tbu: anoc_1_tbu@15189000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x15189000 0x1000>,
				<0x15182200 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x0 0x400>;
			qcom,iova-width = <36>;
		};

		anoc_2_tbu: anoc_2_tbu@15191000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x15191000 0x1000>,
				<0x15182208 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x400 0x400>;
			qcom,iova-width = <36>;
		};

		mnoc_sf_0_tbu: mnoc_sf_0_tbu@15199000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x15199000 0x1000>,
				<0x15182210 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x800 0x400>;
			qcom,iova-width = <36>;
		};

		mnoc_sf_1_tbu: mnoc_sf_1_tbu@0x151a1000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151A1000 0x1000>,
				<0x15182218 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0xC00 0x400>;
			qcom,iova-width = <36>;
		};

		mdp_00_tbu: mdp_00_tbu@0x151a9000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151A9000 0x1000>,
				<0x15182220 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x1000 0x400>;
			qcom,iova-width = <32>;
		};

		mdp_01_tbu: mdp_01_tbu@0x151b1000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151B1000 0x1000>,
				<0x15182228 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x1400 0x400>;
			qcom,iova-width = <32>;
		};

		mdp_10_tbu: mdp_10_tbu@0x151b9000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151B9000 0x1000>,
			<0x15182230 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x1800 0x400>;
			qcom,iova-width = <32>;
		};

		mdp_11_tbu: mdp_11_tbu@151c1000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151C1000 0x1000>,
				<0x15182238 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x1C00 0x400>;
			qcom,iova-width = <32>;
		};

		nsp_00_tbu: nsp_00_tbu@151c9000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151C9000 0x1000>,
				<0x15182240 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x2000 0x400>;
			qcom,iova-width = <32>;
		};

		nsp_01_tbu: nsp_01_tbu@151d1000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151D1000 0x1000>,
				<0x15182248 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x2400 0x400>;
			qcom,iova-width = <32>;
		};

		nsp_10_tbu: nsp_10_tbu@151d9000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151D9000 0x1000>,
				<0x15182250 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x2800 0x400>;
			qcom,iova-width = <32>;
		};

		nsp_11_tbu: nsp_11_tbu@151e1000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151E1000 0x1000>,
				<0x15182258 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x2C00 0x400>;
			qcom,iova-width = <32>;
		};

		lpass_tbu: lpass_tbu@151e9000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151E9000 0x1000>,
				<0x15182260 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x3000 0x400>;
			qcom,iova-width = <32>;
		};

		cam_tbu: cam_tbu@151f1000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151F1000 0x1000>,
				<0x15182268 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x3400 0x400>;
			qcom,iova-width = <32>;
		};

		gpdsp_sail_ss_tbu: gpdsp_sail_ss_tbu@151f9000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x151F9000 0x1000>,
				<0x15182270 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x3800 0x400>;
			qcom,iova-width = <32>;
		};

	};

	kgsl_smmu: kgsl-smmu@3da0000 {
		compatible = "qcom,sa8755p-v500", "arm,mmu-500";
		reg = <0x0 0x3da0000 0x0 0x20000>,
		      <0x0 0x3dca000 0x0 0x28>;
		reg-names = "base", "tcu-base";
		#iommu-cells = <2>;
		qcom,skip-init;
		qcom,use-3-lvl-tables;
		qcom,split-tables;
		#global-interrupts = <2>;
		#size-cells = <1>;
		#address-cells = <1>;
		#tcu-testbus-version = <1>;
		ranges;
		dma-coherent;
		#if 0
		power-domains = <&gpucc GPU_CC_CX_GDSC>;

		clocks = <&gcc GCC_GPU_MEMNOC_GFX_CLK>,
			<&gcc GCC_GPU_SNOC_DVM_GFX_CLK>,
			<&gpucc GPU_CC_AHB_CLK>,
			<&gpucc GPU_CC_HLOS1_VOTE_GPU_SMMU_CLK>,
			<&gpucc GPU_CC_CX_GMU_CLK>,
			<&gpucc GPU_CC_HUB_CX_INT_CLK>,
			<&gpucc GPU_CC_HUB_AON_CLK>;

		clock-names = "gcc_gpu_memnoc_gfx",
			"gcc_gpu_snoc_dvm_gfx",
			"gpu_cc_ahb",
			"gpu_cc_hlos1_vote_gpu_smmu_clk",
			"gpu_cc_cx_gmu_clk",
			"gpu_cc_hub_cx_int_clk",
			"gpu_cc_hub_aon_clk";
		#endif
		interrupts = <GIC_SPI 673 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 674 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 678 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 679 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 680 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 681 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 682 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 683 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 684 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 685 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 686 IRQ_TYPE_LEVEL_HIGH>,
			<GIC_SPI 687 IRQ_TYPE_LEVEL_HIGH>;

		gfx_0_tbu: gfx_0_tbu@3dd1000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x3DD1000 0x1000>,
				<0x3DCA200 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x0 0x400>;
			qcom,iova-width = <49>;
		};

		gfx_1_tbu: gfx_1_tbu@3dd3000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x3DD3000 0x1000>,
				<0x3DCA208 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x400 0x400>;
			qcom,iova-width = <49>;
		};

		gfx_2_tbu: gfx_2_tbu@3dd9000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x3DD9000 0x1000>,
				<0x3DCB200 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0x800 0x400>;
			qcom,iova-width = <49>;
		};

		gfx_3_tbu: gfx_3_tbu@3ddb000 {
			compatible = "qcom,qsmmuv500-tbu";
			reg = <0x3DDB000 0x1000>,
				<0x3DCB208 0x8>;
			reg-names = "base", "status-reg";
			qcom,stream-id-range = <0xC00 0x400>;
			qcom,iova-width = <49>;
		};
	};

	scm_user_intf_device {
		compatible = "qcom,scm-user-intf";
		scm_dev = "scm";
		status = "ok";
	};

	kiumd {
		compatible = "qcom,kiumd";
		qcom,dev-name = "kiumd";
		status = "ok";
	};

	ipcc: mailbox@408000 {
		compatible = "qcom,sa8775p-ipcc", "qcom,ipcc";
		reg = <0x0 0x408000 0x0 0x1000>;
		interrupts = <GIC_SPI 229 IRQ_TYPE_LEVEL_HIGH>;
		interrupt-controller;
		#interrupt-cells = <3>;
		#mbox-cells = <2>;
	};

	ipcc_test1@408000 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x408000 0x0 0x1000>;
		interrupts-extended = <&ipcc IPCC_CLIENT_APSS IPCC_MPROC_SIGNAL_GLINK_QMP
					 IRQ_TYPE_EDGE_RISING>;
		iommus = <&apps_smmu 0x2a00 0x0>;
	};

	ipcc_test2@408000 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x408000 0x0 0x1000>;
		interrupts-extended = <&ipcc IPCC_CLIENT_APSS IPCC_MPROC_SIGNAL_GLINK_QMP
					 IRQ_TYPE_EDGE_RISING>;
		iommus = <&apps_smmu 0x5e0 0x0>;
	};

	umd_glink@780000 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x780000 0x0 0x10000>,
			  <0x0 0x90900000 0x0 0x200000>,
			  <0x0 0x408000 0x0 0x1000>;

		interrupts-extended =
			<&ipcc IPCC_CLIENT_LPASS IPCC_MPROC_SIGNAL_GLINK_QMP IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_CDSP IPCC_MPROC_SIGNAL_GLINK_QMP IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_NSP1 IPCC_MPROC_SIGNAL_GLINK_QMP IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_SLPI IPCC_MPROC_SIGNAL_GLINK_QMP IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_GPDSP0 IPCC_MPROC_SIGNAL_GLINK_QMP IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_GPDSP1 IPCC_MPROC_SIGNAL_GLINK_QMP IRQ_TYPE_EDGE_RISING>;

		iommus = <&apps_smmu 0x5e0 0x0>;
		dma-coherent;
	};

	umd_pil@408000 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x408000 0x0 0x1000>,
		      <0x0 0x90900000 0x0 0x200000>,
		      <0x0 0x780000 0x0 0x10000>,
		      <0x0 0x146d8000 0x0 0x1000>,
		      <0x0 0xadc00000 0x0 0x300000>,
		      <0x0 0x95c00000 0x0 0x1e00000>,
		      <0x0 0x9b800000 0x0 0x1e00000>,
		      <0x0 0x9fc00000 0x0 0x700000>,
		      <0x0 0x9d700000 0x0 0x1e00000>,
		      <0x0 0x97b00000 0x0 0x1e00000>,
		      <0x0 0x99900000 0x0 0x1e00000>,
		      <0x0 0x9d600000 0x0 0x800000>;

		interrupts-extended =
			<&ipcc IPCC_CLIENT_LPASS IPCC_MPROC_SIGNAL_SMP2P IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_CDSP IPCC_MPROC_SIGNAL_SMP2P IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_NSP1 IPCC_MPROC_SIGNAL_SMP2P IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_SLPI IPCC_MPROC_SIGNAL_SMP2P IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_GPDSP0 IPCC_MPROC_SIGNAL_SMP2P IRQ_TYPE_EDGE_RISING>,
			<&ipcc IPCC_CLIENT_GPDSP1 IPCC_MPROC_SIGNAL_SMP2P IRQ_TYPE_EDGE_RISING>,
			<&intc GIC_SPI 486 IRQ_TYPE_LEVEL_HIGH>,
			<&intc GIC_SPI 578 IRQ_TYPE_LEVEL_HIGH>,
			<&intc GIC_SPI 887 IRQ_TYPE_LEVEL_HIGH>,
			<&intc GIC_SPI 768 IRQ_TYPE_LEVEL_HIGH>,
			<&intc GIC_SPI 624 IRQ_TYPE_LEVEL_HIGH>;

		iommus = <&apps_smmu 0x2a00 0x0>;
		dma-coherent;
	};

	vfio_timer@17c23000 {
		compatible = "qcom,vfio-platform";
		interrupts = <GIC_SPI 9 IRQ_TYPE_LEVEL_HIGH>;
		reg = <0x0 0x17c23000 0x0 0x1000>;
		iommus = <&apps_smmu 0x5e0 0x0>;
	};

	cdsp0_cb@1 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x1 0x0 0x0>;
		iommus = <&apps_smmu 0x21c1 0x0>,
			 <&apps_smmu 0x25c1 0x0>,
			 <&apps_smmu 0x2161 0x0>,
			 <&apps_smmu 0x2561 0x0>,
			 <&apps_smmu 0x2141 0x0>,
			 <&apps_smmu 0x2541 0x0>,
			 <&apps_smmu 0x21e1 0x0>,
			 <&apps_smmu 0x25e1 0x0>,
			 <&apps_smmu 0x2181 0x0>,
			 <&apps_smmu 0x2581 0x0>;
		dma-coherent;
	};

	cdsp0_cb@2 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x2 0x0 0x0>;
		iommus = <&apps_smmu 0x21c2 0x0>,
			 <&apps_smmu 0x25c2 0x0>,
			 <&apps_smmu 0x2162 0x0>,
			 <&apps_smmu 0x2562 0x0>,
			 <&apps_smmu 0x2142 0x0>,
			 <&apps_smmu 0x2542 0x0>,
			 <&apps_smmu 0x21e2 0x0>,
			 <&apps_smmu 0x25e2 0x0>,
			 <&apps_smmu 0x2182 0x0>,
			 <&apps_smmu 0x2582 0x0>;
		dma-coherent;
	};

	cdsp0_cb@3 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x3 0x0 0x0>;
		iommus = <&apps_smmu 0x21c3 0x0>,
			 <&apps_smmu 0x25c3 0x0>,
			 <&apps_smmu 0x2163 0x0>,
			 <&apps_smmu 0x2563 0x0>,
			 <&apps_smmu 0x2143 0x0>,
			 <&apps_smmu 0x2543 0x0>,
			 <&apps_smmu 0x21e3 0x0>,
			 <&apps_smmu 0x25e3 0x0>,
			 <&apps_smmu 0x2183 0x0>,
			 <&apps_smmu 0x2583 0x0>;
		dma-coherent;
	};

	cdsp0_cb@4 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x4 0x0 0x0>;
		iommus = <&apps_smmu 0x21c4 0x0>,
			 <&apps_smmu 0x25c4 0x0>,
			 <&apps_smmu 0x2164 0x0>,
			 <&apps_smmu 0x2564 0x0>,
			 <&apps_smmu 0x2144 0x0>,
			 <&apps_smmu 0x2544 0x0>,
			 <&apps_smmu 0x21e4 0x0>,
			 <&apps_smmu 0x25e4 0x0>,
			 <&apps_smmu 0x2184 0x0>,
			 <&apps_smmu 0x2584 0x0>;
		dma-coherent;
	};

	cdsp1_cb@1 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x1 0x0 0x0>;
		iommus = <&apps_smmu 0x2961 0x0>,
			 <&apps_smmu 0x2d61 0x0>,
			 <&apps_smmu 0x29c1 0x0>,
			 <&apps_smmu 0x2dc1 0x0>,
			 <&apps_smmu 0x2941 0x0>,
			 <&apps_smmu 0x2d41 0x0>,
			 <&apps_smmu 0x2981 0x0>,
			 <&apps_smmu 0x2d81 0x0>,
			 <&apps_smmu 0x29e1 0x0>,
			 <&apps_smmu 0x2de1 0x0>;
		dma-coherent;
	};

	cdsp1_cb@2 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x2 0x0 0x0>;
		iommus = <&apps_smmu 0x2962 0x0>,
			 <&apps_smmu 0x2d62 0x0>,
			 <&apps_smmu 0x29c2 0x0>,
			 <&apps_smmu 0x2dc2 0x0>,
			 <&apps_smmu 0x2942 0x0>,
			 <&apps_smmu 0x2d42 0x0>,
			 <&apps_smmu 0x29e2 0x0>,
			 <&apps_smmu 0x2de2 0x0>,
			 <&apps_smmu 0x2982 0x0>,
			 <&apps_smmu 0x2d82 0x0>;
		dma-coherent;
	};

	cdsp1_cb@3 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x3 0x0 0x0>;
		iommus = <&apps_smmu 0x2963 0x0>,
			 <&apps_smmu 0x2d63 0x0>,
			 <&apps_smmu 0x29c3 0x0>,
			 <&apps_smmu 0x2dc3 0x0>,
			 <&apps_smmu 0x2943 0x0>,
			 <&apps_smmu 0x2d43 0x0>,
			 <&apps_smmu 0x29e3 0x0>,
			 <&apps_smmu 0x2de3 0x0>,
			 <&apps_smmu 0x2983 0x0>,
			 <&apps_smmu 0x2d83 0x0>;
		dma-coherent;
	};

	cdsp1_cb@4 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x4 0x0 0x0>;
		iommus = <&apps_smmu 0x2964 0x0>,
			 <&apps_smmu 0x2d64 0x0>,
			 <&apps_smmu 0x29c4 0x0>,
			 <&apps_smmu 0x2dc4 0x0>,
			 <&apps_smmu 0x2944 0x0>,
			 <&apps_smmu 0x2d44 0x0>,
			 <&apps_smmu 0x29e4 0x0>,
			 <&apps_smmu 0x2de4 0x0>,
			 <&apps_smmu 0x2984 0x0>,
			 <&apps_smmu 0x2d84 0x0>;
		dma-coherent;
	};

	adsp_cb@2 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x2 0x0 0x0>;
		iommus = <&apps_smmu 0x3003 0x0>;
		dma-coherent;
	};

	adsp_cb@3 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x3 0x0 0x0>;
		iommus = <&apps_smmu 0x3004 0x0>;
		dma-coherent;
	};

	adsp_cb@4 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x4 0x0 0x0>;
		iommus = <&apps_smmu 0x3005 0x0>;
		dma-coherent;
	};

	gpdsp0_cb@1 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x1 0x0 0x0>;
		iommus = <&apps_smmu 0x38a1 0x0>;
		dma-coherent;
	};

	gpdsp0_cb@3 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x3 0x0 0x0>;
		iommus = <&apps_smmu 0x38a3 0x0>;
		dma-coherent;
	};

	gpdsp1_cb@1 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x1 0x0 0x0>;
		iommus = <&apps_smmu 0x38c1 0x0>;
		dma-coherent;
	};

	gpdsp1_cb@3 {
		compatible = "qcom,vfio-platform";
		reg = <0x0 0x3 0x0 0x0>;
		iommus = <&apps_smmu 0x38c3 0x0>;
		dma-coherent;
	};

	tcsr_mutex: hwlock@1f40000 {
		compatible = "qcom,tcsr-mutex";
		reg = <0x0 0x1f40000 0x0 0x20000>;
		#hwlock-cells = <1>;
	};

	secure_buffer: qcom,secure-buffer {
		compatible = "qcom,secure-buffer";
	};

	auto_vm: qcom,auto_vm@d5100000 {
		reg = <0x0 0xd5100000 0x0 0x1300000>;
		vm_name = "autoghgvm";
		shared-buffers = <&auto_vm_vblk0_ring &auto_vm_vblk1_ring &auto_vm_vblk2_ring
				 &auto_vm_vblk3_ring &auto_vm_vblk4_ring &auto_vm_vnet0_ring
				 &auto_vm_vhab0_ring &auto_vm_vhab1_ring &auto_vm_vhab2_ring
				 &auto_vm_vhab3_ring &auto_vm_vhab4_ring &auto_vm_vhab5_ring
				 &auto_vm_input0_ring &auto_vm_vscmi_ring &auto_vm_vconsole_ring
				 &auto_vm_input1_ring &auto_vm_resv_ring &auto_vm_swiotlb>;
	};

	auto_vm_be0: auto_vm_be0@10 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x10>;
	};

	auto_vm_be1: auto_vm_be1@11 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x11>;
	};

	auto_vm_be2: auto_vm_be2@13 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x13>;
	};

	auto_vm_be3: auto_vm_be3@14 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x14>;
	};

	auto_vm_be4: auto_vm_be4@15 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x15>;
	};

	auto_vm_be5: auto_vm_be5@16 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x16>;
	};

	auto_vm_be6: auto_vm_be6@17 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x17>;
	};

	auto_vm_be7: auto_vm_be7@18 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x18>;
	};

	auto_vm_be8: auto_vm_be8@19 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x19>;
	};

	auto_vm_be9: auto_vm_be9@1a {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x1a>;
	};

	auto_vm_be10: auto_vm_be10@1b {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x1b>;
	};

	auto_vm_be11: auto_vm_be11@1c {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x1c>;
	};

	auto_vm_be12: auto_vm_be12@1d {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x1d>;
	};

	auto_vm_be13: auto_vm_be13@1e {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x1e>;
	};

	auto_vm_be14: auto_vm_be14@1f {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x1f>;
	};

	auto_vm_be15: auto_vm_be15@20 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x20>;
	};

	auto_vm_be16: auto_vm_be16@21 {
		qcom,vm = <&auto_vm>;
		qcom,label = <0x21>;
	};

	auto_vm_0: gh-secure-vm-loader@0 {
		compatible = "qcom,gh-secure-vm-loader";
		qcom,pas-id = <44>;
		qcom,vmid = <52>;
		qcom,firmware-name = "autoghgvm";
		qcom,firmware-index = <0>;
		memory-region = <&auto_vm_0_mem_0 &auto_vm_0_mem_1 &auto_vm_0_mem_2
				&auto_vm_0_mem_3 &auto_vm_0_mem_4 &auto_vm_0_mem_5
				&auto_vm_0_mem_6>;
		virtio-backends = <&auto_vm_be0 &auto_vm_be1 &auto_vm_be2 &auto_vm_be3 &auto_vm_be4
				&auto_vm_be5 &auto_vm_be6 &auto_vm_be7 &auto_vm_be8 &auto_vm_be9
				&auto_vm_be10 &auto_vm_be11 &auto_vm_be12 &auto_vm_be13
				&auto_vm_be14 &auto_vm_be15 &auto_vm_be16>;
		enable-gvm-dump;
	};
};
